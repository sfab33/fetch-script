#!/bin/sh

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# Underline
UBlack='\033[4;30m'       # Black
URed='\033[4;31m'         # Red
UGreen='\033[4;32m'       # Green
UYellow='\033[4;33m'      # Yellow
UBlue='\033[4;34m'        # Blue
UPurple='\033[4;35m'      # Purple
UCyan='\033[4;36m'        # Cyan
UWhite='\033[4;37m'       # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# High Intensity
IBlack='\033[0;90m'       # Black
IRed='\033[0;91m'         # Red
IGreen='\033[0;92m'       # Green
IYellow='\033[0;93m'      # Yellow
IBlue='\033[0;94m'        # Blue
IPurple='\033[0;95m'      # Purple
ICyan='\033[0;96m'        # Cyan
IWhite='\033[0;97m'       # White

# Bold High Intensity
BIBlack='\033[1;90m'      # Black
BIRed='\033[1;91m'        # Red
BIGreen='\033[1;92m'      # Green
BIYellow='\033[1;93m'     # Yellow
BIBlue='\033[1;94m'       # Blue
BIPurple='\033[1;95m'     # Purple
BICyan='\033[1;96m'       # Cyan
BIWhite='\033[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\033[0;100m'   # Black
On_IRed='\033[0;101m'     # Red
On_IGreen='\033[0;102m'   # Green
On_IYellow='\033[0;103m'  # Yellow
On_IBlue='\033[0;104m'    # Blue
On_IPurple='\033[0;105m'  # Purple
On_ICyan='\033[0;106m'    # Cyan
On_IWhite='\033[0;107m'   # White



banner() {

    # Get Infos
    host="$(hostname)"

    # Decide Banner Color
    case $(dnsdomainname) in

        "srv.fabi.lan")
            bannercolor=${IPurple}
            ;;

        "dmz.fabi.lan")
            bannercolor=${IYellow}
            ;;

        "fabi.lan")
            bannercolor=${IBlue}
            ;;

        *)
            bannercolor=${IWhite}
            ;;
    esac

    # Print Information
    printf "${bannercolor}$(figlet -t ${host})"
    printf "${Color_Off}\n"
}

spacer_line() {

    # $1 - Test Color (String)

    printf "$1%s\n" "$(uname -a)"
    printf "${Color_Off}\n"

}

general_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    if [ "$(dnsdomainname)" != "" ]
    then
        fqdn="$(hostname).$(dnsdomainname)"
    else
        fqdn="$(hostname)"
    fi
    uptime="$(uptime -p | sed 's/up //')"
    packages="$(dpkg -l | grep -c ^i)"
    shell=$(basename "${SHELL}")
    processes=$(ps -aux | wc -l)
    birth_date=$(cat /etc/birth-certificate 2> /dev/null)
    distro=$(lsb_release -d | awk '{print $2,$3,$4,$5,$6}')

    # Print Information
    printf "$1%s${Color_Off}\n" "General:"
    printf "$2%-9s $2%s\n" "Distro" "${distro}"
    printf "$2%-9s $2%s\n" "FQDN" "${fqdn}"
    printf "$2%-9s $2%s\n" "Uptime" "${uptime}"
    printf "$2%-9s $2%s\n" "Packages" "${packages}"
    printf "$2%-9s $2%s\n" "Shell" "${shell}"
    printf "$2%-9s $2%s\n" "Processes" "${processes}"
    if [ "$birth_date" != "" ]
    then
        printf "$2%-9s $2%s\n" "Birthdate" "${birth_date}"
    fi
    printf "${Color_Off}\n"

}

interfaces_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    interfaces=$(ip --brief address show | tr -s ' ' | cut -d ' ' -f 1,2,3 | awk '{print $1,$3,$2}' | column -t)

    # Print Information
    printf "$1%s${Color_Off}\n" "Interfaces:"
    printf "$2%s\n" "${interfaces}"
    printf "${Color_Off}\n"
}

ports_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    ports=$({ ss -l -4 -H ; ss -l -6 -H; } | awk '{print $1,$2,$5}')

    #Print Information
    printf "$1%s${Color_Off}\n" "Open Ports:"
    printf "$2%s\n" "${ports}"
    printf "${Color_Off}\n"

}

users_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    users=$(who -u | awk '{ print $1"@"$2,SINCE,$4"."$3","$5,$8}' | column -t)

    # Print Information
    printf "$1%s${Color_Off}\n" "Logged-In Users:"
    printf "$2%s\n" "${users}"
    printf "${Color_Off}\n"

}

performance_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    cpu_5min_avg=$(cat /proc/loadavg | awk '{ print $1"%" }')
    memory=$(free | grep Mem | awk '{printf "%3.1f%s\n", ($3/$2 * 100.0), "%"}')
    swap=$(cat /proc/swaps | sed 1d |awk '{printf "%3.1f%s\n", ($4/$3*100), "%"}')

    # Print Information
    printf "$1%s${Color_Off}\n" "Performance:"
    #printf "$2%-9s $2%s\n" "CPU%" "${cpu_5min_avg}"     # Is not accurate --> find better solution
    printf "$2%-9s $2%s\n" "Memory%" "${memory}"
    printf "$2%-9s $2%s\n" "Swap%" "${swap}"
    printf "${Color_Off}\n"

}

diskusage_info() {

    # $1 - Title Color
    # $2 - Text Color

    # Get Information
    diskusage=$(df | sed 1d | awk '{print $6,$1,$5}'| column -t)

    # Print Information
    printf "$1%s${Color_Off}\n" "Partitions:"
    printf "$2%s\n" "${diskusage}"
    printf "${Color_Off}\n"

}

print_colors() {

    printf "${Black}%-11s ${Color_Off}${BBlack}%-11s ${Color_Off}${UBlack}%-11s ${Color_Off}${IBlack}%-11s ${Color_Off}${BIBlack}%-11s ${Color_Off}${White}${ON_Black}%-11s ${Color_Off}${White}${ON_IBlack}%-11s ${Color_Off}\n" "Black" "BBlack" "UBlack" "IBlack" "BIBlack" "ON_Black" "ON_IBlack"
    printf "${Color_Off}"
    printf "${Red}%-11s ${Color_Off}${BRed}%-11s ${Color_Off}${URed}%-11s ${Color_Off}${IRed}%-11s ${Color_Off}${BIRed}%-11s ${Color_Off}${White}${ON_Red}%-11s ${Color_Off}${White}${ON_IRed}%-11s ${Color_Off}\n" "Red" "BRed" "URed" "IRed" "BIRed" "ON_Red" "ON_IRed"
    printf "${Color_Off}"
    printf "${Green}%-11s ${Color_Off}${BGreen}%-11s ${Color_Off}${UGreen}%-11s ${Color_Off}${IGreen}%-11s ${Color_Off}${BIGreen}%-11s ${Color_Off}${White}${ON_Green}%-11s ${Color_Off}${White}${ON_IGreen}%-11s ${Color_Off}\n" "Green" "BGreen" "UGreen" "IGreen" "BIGreen" "ON_Green" "ON_IGreen"
    printf "${Color_Off}"
    printf "${Yellow}%-11s ${Color_Off}${BYellow}%-11s ${Color_Off}${UYellow}%-11s ${Color_Off}${IYellow}%-11s ${Color_Off}${BIYellow}%-11s ${Color_Off}${White}${ON_Yellow}%-11s ${Color_Off}${White}${ON_IYellow}%-11s ${Color_Off}\n" "Yellow" "BYellow" "UYellow" "IYellow" "BIYellow" "ON_Yellow" "ON_IYellow"
    printf "${Color_Off}"
    printf "${Blue}%-11s ${Color_Off}${BBlue}%-11s ${Color_Off}${UBlue}%-11s ${Color_Off}${IBlue}%-11s ${Color_Off}${BIBlue}%-11s ${Color_Off}${White}${ON_Blue}%-11s ${Color_Off}${White}${ON_IBlue}%-11s ${Color_Off}\n" "Blue" "BBlue" "UBlue" "IBlue" "BIBlue" "ON_Blue" "ON_IBlue"
    printf "${Color_Off}"
    printf "${Purple}%-11s ${Color_Off}${BPurple}%-11s ${Color_Off}${UPurple}%-11s ${Color_Off}${IPurple}%-11s ${Color_Off}${BIPurple}%-11s ${Color_Off}${White}${ON_Purple}%-11s ${Color_Off}${White}${ON_IPurple}%-11s ${Color_Off}\n" "Purple" "BPurple" "UPurple" "IPurple" "BIPurple" "ON_Purple" "ON_IPurple"
    printf "${Color_Off}"
    printf "${Cyan}%-11s ${Color_Off}${BCyan}%-11s ${Color_Off}${UCyan}%-11s ${Color_Off}${ICyan}%-11s ${Color_Off}${BICyan}%-11s ${Color_Off}${White}${ON_Cyan}%-11s ${Color_Off}${White}${ON_ICyan}%-11s ${Color_Off}\n" "Cyan" "BCyan" "UCyan" "ICyan" "BICyan" "ON_Cyan" "ON_ICyan"
    printf "${Color_Off}"
    printf "${White}%-11s ${Color_Off}${BWhite}%-11s ${Color_Off}${UWhite}%-11s ${Color_Off}${IWhite}%-11s ${Color_Off}${BIWhite}%-11s ${Color_Off}${White}${ON_White}%-11s ${Color_Off}${White}${ON_IWhite}%-11s ${Color_Off}\n" "White" "BWhite" "UWhite" "IWhite" "BIWhite" "ON_White" "ON_IWhite"
    printf "${Color_Off}\n"

}

display_help() {

    printf "\n"
    printf "%s\n" "-b - Show banner"
    printf "%s\n" "-l - Show spacer line"
    printf "%s\n" "-g - Show general information"
    printf "%s\n" "-i - Show interface information"
    printf "%s\n" "-p - Show ports information"
    printf "%s\n" "-u - Show users information"
    printf "%s\n" "-s - Show performance information"
    printf "%s\n" "-d - Show partition usage"
    printf "%s\n" "-c - Print available colors"
    printf "%s\n" "-h - Show this message"
    printf "\n"
}


if [ $# -eq 0 ]
then
    display_help
else
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' '.'
fi

while getopts blgsipudch flag
do
    case "${flag}" in
        b) banner | sed "s/^/  /";;
        l) spacer_line $IYellow | sed "s/^/  /";;
        g) general_info $UCyan $IWhite | sed "s/^/  /";;
        s) performance_info $UCyan $IWhite | sed "s/^/  /";;
        i) interfaces_info $UCyan $IWhite | sed "s/^/  /";;
        p) ports_info $UCyan $IWhite | sed "s/^/  /";;
        u) users_info $UCyan $IWhite | sed "s/^/  /";;
        d) diskusage_info $UCyan $IWhite | sed "s/^/  /";;
        c) print_colors;;
        *|h) display_help;;

    esac
done

if [ $# -ne 0 ]
then
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' '.'
    printf '\n'
fi

printf "${Color_Off}"

exit 0
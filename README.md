# fetch-script

This fetch script is a shell-script, that fetches the following Information:

![](img/all-options.png)

## Usage

`.\show_banner.sh [-blgipusdch]`

To determine which catogiers will be shown the following flags can be passed:

- -b -> Show banner
- -l -> Show spacer line
- -g -> Show general information
- -i -> Show interface information
- -p -> Show ports information
- -u -> Show users information
- -s -> Show performance information
- -d -> Show partition usage
- -c -> Print available colors
- -h -> Show help message

## Functions

### Show banner (-b)

![](img/show_banner.png)

This shows a large text of the current hostname.

In order for this to work the package `figlet` is required.

The text will change color, if the domainname contains one of the keywords srv,dmz or lan.
This is a personal customization to fit my needs.

### Show spacer line (-l)

![](img/show_spacer.png)

The spacer line is both a spacer and common information about the host.

Basically it is just `uname -a` in yellow.

### Show general information (-g)

![](img/show_gneral.png)

The general information show:

- fqdn
- uptime
- installed packages (apt)
- current shell
- running processes
- birthdate of host

### Show interface information (-i)

![](img/show_interfaces.png)

This section shows the configured IP Adresses along with their status.

### Show ports information (-p)

![](img/show_open_ports.png)

This shows all open ports on TCP/UDP sockets on IPv4 and IPv6.

Please note, that this only shows the ports which the current user is priviliged to read.

### Show users information (-u)

![](img/show_users.png)

This section lists the currently logged-in users, when they have logged in and, when available, their IP-Address.

### Show performance information (-s)

![](img/show_performance.png)

This shows some metrics about how the system is performing.

Currently it lists the ram- and swap-usage. The CPU usage is not yet implemented, as the methods to get it are unreliable and/or slow.

### Show partition usage (-d)

![](img/show_partitions.png)

This part lists all Mountpoints along with their partition and usage in percent.

### Print available colors (-c)

This displays all color variations available for the script.

It was created for debugging purposes.


## Installation

### Dependencies

The Dependencies needed are:
- figlet

### Cloning

The Repo can be cloned with:
```
git clone https://gitlab.com/sfab33/fetch-script.git
```

### Birth-certificate

To use the Birthdate function, a file has to be created in your /etc with the date of the machines creation in it:

```
sudo echo <date> > /etc/birth-certificate
```

Note, that the script would work without this step.
The section "Birthdate" would be hidden in the fetch.

## ToDo
- Proper error handling
- Free choice of category-order
- Redo help-message
- Implement CPU monitoring